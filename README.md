# Projet Big Data

## Maxime Stawski - Axelle Mathias

### M2 WEBSCI

## Us Accidents

Nous avons utilisé les données d'un jeu de données sur Kaggle, disponible à cette addresse : https://www.kaggle.com/datasets/sobhanmoosavi/us-accidents.


## Données

Les données ont été préalablement converti en .parquet et disponible à la racine du projet dans le dossier "ressources", chaque fichier correspond à une question, donc
pour exploiter les données qui répondent à la question 1 on utilise "data1.parquet". 

Les fichiers de données sont disponibles ici : https://drive.google.com/drive/folders/1FJPP7ySxqs84XUoyoBowxPDJn53Ci_nI?usp=sharing
package edu.fun.spark

import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.functions._

object Main {
  def main(args: Array[String]): Unit = {
    // Créer une session Spark
    val spark = SparkSession.builder()
      .appName("Accident Analysis")
      .getOrCreate()

    // Lire le fichier Parquet
    val accidentsParquetPath = args(0)
    val file = spark.read.parquet(accidentsParquetPath).cache()

    // QUESTIONS
    //val distribution = question1(spark, file)
    //val distribution = question2(spark,file)
    //val distribution = question3(spark,file)

    // Enregistrer les résultats au format CSV
    //saveToCSV(distribution, "resQuestion4")

    // Afficher les résultats
    //distribution.show()

    // Fermer la session Spark
    spark.stop()
  }

  /**
   * QUESTION 1 : Quelle est la distribution géographique des accidents ?
   *
   * @param spark
   * @param accidentsDF
   * @return
   */
  def question1(spark: SparkSession, accidentsDF: DataFrame): DataFrame = {
    // Sélectionner les colonnes pertinentes
    val selectedColumns = accidentsDF.select("State")

    val accidentDistribution = selectedColumns.groupBy("State").count().orderBy(desc("count"))

    // Retourner les résultats
    accidentDistribution

  }

  /**
   * QUESTION 2 : Quels sont les moments les plus fréquents pour les accidents de la route ?
   *
   * @param spark
   * @param accidentsDF
   * @return
   */

  def question2(spark: SparkSession, accidentsDF: DataFrame): DataFrame = {

    // Convertir les colonnes de date en format Timestamp
    val dfTimestamp = accidentsDF.withColumn("Start_Time", to_timestamp(col("Start_Time")))
      .withColumn("Sunrise_Sunset", when(col("Sunrise_Sunset") === "Night", "Nuit").otherwise("Jour"))

    // Ajouter une colonne pour déterminer s'il fait nuit ou jour
    val dfDayNight = dfTimestamp.withColumn("Moment_Journee", when(col("Sunrise_Sunset") === "Nuit", "Nuit").otherwise("Jour"))

    // Extraire l'heure de l'accident
    val dfWithHour = dfDayNight.withColumn("Hour", hour(col("Start_Time")))

    // Calculer le nombre d'accidents par heure
    val accidentsPerHour = dfWithHour.groupBy("Hour").count()

    // Calculer le pourcentage d'accidents pour chaque heure
    val totalAccidents = accidentsPerHour.agg(sum("count")).first().getLong(0)
    val accidentsPercentage = accidentsPerHour.withColumn("Percentage", (col("count") / totalAccidents) * 100)

    // Afficher les résultats
    accidentsPercentage.show()

    // Sélectionner l'heure avec le plus grand pourcentage
    val mostProbableHour = accidentsPercentage.orderBy(desc("Percentage")).first().getInt(0)

    // Afficher les résultats finaux
    println(s"L'heure avec le plus grand pourcentage d'accidents est : $mostProbableHour")

    // Déterminer s'il fait nuit ou jour au moment de l'accident
    val dayOrNight = dfWithHour.filter(col("Hour") === mostProbableHour).select("Moment_Journee").first().getString(0)

    println(s"Au moment de la plupart des accidents, il fait : $dayOrNight")

    accidentsPercentage
  }




  /**
   * QUESTION 3 : Y a t-il des caractèristiques communes associées ayx accidents graves ?
   *
   * @param spark
   * @param accidentsDF
   * @return
   */
  def question3(spark: SparkSession, accidentDF: DataFrame): Unit = {
    // Sélectionner les colonnes pertinentes
    val dfSelected = accidentDF.select("Severity", "Visibility(mi)", "Wind_Speed(mph)", "Temperature(F)", "Humidity(%)", "Pressure(in)")


    val range = 1 to 4
    for (num <- range) {

      // Filtrer les accidents graves (Severity = 4)
      val dfGraveAccidents = dfSelected.filter(col("Severity") === num)

      // Calculer les valeurs moyennes des caractéristiques pour les accidents graves
      val avgCharacteristics = dfGraveAccidents.groupBy().agg(
        avg("Visibility(mi)").as("Avg_Visibility"),
        avg("Wind_Speed(mph)").as("Avg_Wind_Speed"),
        avg("Temperature(F)").as("Avg_Temperature"),
        avg("Humidity(%)").as("Avg_Humidity"),
        avg("Pressure(in)").as("Avg_Pressure")
      ).collect()(0)


      println("[SEVERITY "+num+"] Caractéristiques moyennes des accidents graves :")
      println(s"Visibility : ${avgCharacteristics.getDouble(0)}")
      println(s"Wind Speed : ${avgCharacteristics.getDouble(1)}")
      println(s"Temperature : ${avgCharacteristics.getDouble(2)}")
      println(s"Humidity : ${avgCharacteristics.getDouble(3)}")
      println(s"Pressure : ${avgCharacteristics.getDouble(4)}")

    }

  }

  /**
   * QUESTION 4 : Y a-t-il une diminution ou une augmentation du nombre d’accidents au fil du temps ?
   *
   * @param spark
   * @param accidentsDF
   * @return
   */
  def question4(spark: SparkSession, accidentsDF: DataFrame): Unit = {

    // Convertir les colonnes de date en format Timestamp
    val dfTimestamp = accidentsDF
      .withColumn("Start_Time", to_timestamp(col("Start_Time")))
      .withColumn("End_Time", to_timestamp(col("End_Time")))

    // Extraire l'année de la colonne "Start_Time"
    val dfWithYear = dfTimestamp.withColumn("Year", year(col("Start_Time")))

    // Calculer le nombre d'accidents par année
    val accidentsPerYear = dfWithYear.groupBy("Year").count()

    // Calculer le pourcentage d'accidents pour chaque année
    val totalAccidents = accidentsPerYear.agg(sum("count")).first().getLong(0)
    val accidentsPercentage = accidentsPerYear.withColumn("Percentage", (col("count") / totalAccidents) * 100)

    // Afficher les résultats
    accidentsPercentage.show()

    // Collecter les résultats pour créer une liste
    val resultsList = accidentsPercentage.collect().map(row =>
      (row.getAs[Int]("Year"), row.getAs[Long]("count"), row.getAs[Double]("Percentage"))
    )

    // Afficher la liste du nombre d'accidents par année avec les pourcentages
    println("Liste du nombre d'accidents par année avec les pourcentages :")
    resultsList.foreach { case (year, count, percentage) =>
      println(s"Année : $year, Nombre d'accidents : $count, Pourcentage : $percentage%")
    }
  }


  /**
   *  Permet d'enregister les résultats dans un csv
   *
   * @param dataFrame
   * @param filePath
   */
  def saveToCSV(dataFrame: DataFrame, filePath: String): Unit = {
    // Enregistrer les résultats au format CSV
    dataFrame.write
      .option("header", "true")
      .csv(filePath)
  }
}